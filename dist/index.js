"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConverterBuilder = exports.shuffle = exports.sleep = exports.nullObject = exports.getCleanString = exports.getUnix = exports.compareGeneric = exports.parseNumber = exports.isEmpty = exports.isSimilar = exports.checkSimilar = exports.getTimestamp = exports.getYear = exports.getTimestampFromCreatedAt = exports.clearCode = exports.SIMILARITY_CHECK = void 0;
var string_similarity_1 = __importDefault(require("string-similarity"));
exports.SIMILARITY_CHECK = 0.70;
var defaultFactor = { factor: 1.0, value: 'kg' };
function clearCode(myString, keywords, size) {
    if (keywords === void 0) { keywords = []; }
    try {
        myString = myString.toLowerCase();
        keywords.forEach(function (keyword) {
            myString = myString.replace(keyword, "");
        });
        myString = myString.replace(/\D/g, "");
        var code = parseInt(myString.trim());
        if (isNaN(code)) {
            return '';
        }
        code = code.toString();
        while (code.length < size) {
            code = '0' + code;
        }
        return code;
    }
    catch (error) {
        console.log(error.toString());
        return myString || '';
    }
}
exports.clearCode = clearCode;
function getTimestampFromCreatedAt(created_at) {
    var date = new Date(created_at);
    return ((date.getMonth() > 8) ? (date.getMonth() + 1) : ('0' + (date.getMonth() + 1))) + '/' + ((date.getDate() > 9) ? date.getDate() : ('0' + date.getDate())) + '/' + date.getFullYear();
}
exports.getTimestampFromCreatedAt = getTimestampFromCreatedAt;
function getYear() {
    return new Date().getFullYear().toString();
}
exports.getYear = getYear;
function getTimestamp() {
    return new Date().toLocaleDateString("en-US").toString();
}
exports.getTimestamp = getTimestamp;
function checkSimilar(firstString, secondString) {
    var firstCleaned = getCleanString(firstString).toLowerCase();
    var secondCleaned = getCleanString(secondString).toLowerCase();
    return string_similarity_1.default.compareTwoStrings(firstCleaned, secondCleaned);
}
exports.checkSimilar = checkSimilar;
function isSimilar(firstString, secondString, howSimilar) {
    if (howSimilar === void 0) { howSimilar = exports.SIMILARITY_CHECK; }
    return checkSimilar(firstString, secondString) >= howSimilar;
}
exports.isSimilar = isSimilar;
function isEmpty(value) {
    var _a;
    return (value == undefined || value == null || (value && ((_a = value === null || value === void 0 ? void 0 : value.trim()) === null || _a === void 0 ? void 0 : _a.length) === 0) || (value === null || value === void 0 ? void 0 : value.length) === 0);
}
exports.isEmpty = isEmpty;
function parseNumber(rawNumber) {
    try {
        var n = parseFloat(rawNumber.replace("$", ""));
        if (isNaN(n)) {
            return 0;
        }
        return n;
    }
    catch (error) {
        console.log(error);
        return 0;
    }
}
exports.parseNumber = parseNumber;
function compareGeneric(key) {
    return function (a, b) {
        if (a[key] < b[key]) {
            return -1;
        }
        if (a[key] > b[key]) {
            return 1;
        }
        return 0;
    };
}
exports.compareGeneric = compareGeneric;
function getUnix() {
    return Math.round(+new Date() / 1000).toString();
}
exports.getUnix = getUnix;
function getCleanString(input) {
    try {
        return input.trim() || '';
    }
    catch (error) {
        return '';
    }
}
exports.getCleanString = getCleanString;
function nullObject(original) {
    var args = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        args[_i - 1] = arguments[_i];
    }
    try {
        var object_1 = original;
        args.forEach(function (x) {
            object_1 = object_1[x];
        });
        return object_1;
    }
    catch (error) {
        console.log(error);
        return '';
    }
}
exports.nullObject = nullObject;
function sleep(n) {
    return new Promise(function (resolve, _) {
        setInterval(function () {
            resolve(true);
        }, n * 1000);
    });
}
exports.sleep = sleep;
function shuffle(array) {
    var currentIndex = array.length;
    var temporaryValue = 0;
    var randomIndex = 0;
    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;
        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }
    return array;
}
exports.shuffle = shuffle;
var ConverterBuilder = /** @class */ (function () {
    function ConverterBuilder() {
        this.inputUOM = defaultFactor;
        this.outputUOM = defaultFactor;
        this.inputValue = 0;
    }
    ConverterBuilder.prototype.setInputUOM = function (inputUOM) {
        this.inputUOM = inputUOM;
        return this;
    };
    ConverterBuilder.prototype.setOutputUOM = function (outputUOM) {
        this.outputUOM = outputUOM;
        return this;
    };
    ConverterBuilder.prototype.setValue = function (inputValue) {
        this.inputValue = inputValue;
        return this;
    };
    ConverterBuilder.prototype.calculate = function () {
        return this.inputValue * (this.inputUOM.factor / this.outputUOM.factor);
    };
    return ConverterBuilder;
}());
exports.ConverterBuilder = ConverterBuilder;
