export declare const SIMILARITY_CHECK: number;
export interface FactorInterface {
    factor: number;
    value?: any;
}
export declare function clearCode(myString: any, keywords: string[] | undefined, size: number): string;
export declare function getTimestampFromCreatedAt(created_at: any): string;
export declare function getYear(): string;
export declare function getTimestamp(): string;
export declare function checkSimilar(firstString: string, secondString: string): number;
export declare function isSimilar(firstString: string, secondString: string, howSimilar?: number): boolean;
export declare function isEmpty(value: any): boolean;
export declare function parseNumber(rawNumber: any): number;
export declare function compareGeneric(key: string): Function;
export declare function getUnix(): string;
export declare function getCleanString(input: any): any;
export declare function nullObject(original: any, ...args: any): any;
export declare function sleep(n: number): Promise<boolean>;
export declare function shuffle(array: Array<any>): Array<any>;
export declare class ConverterBuilder {
    private inputUOM;
    private outputUOM;
    private inputValue;
    setInputUOM(inputUOM: FactorInterface): ConverterBuilder;
    setOutputUOM(outputUOM: FactorInterface): ConverterBuilder;
    setValue(inputValue: number): ConverterBuilder;
    calculate(): number;
}
