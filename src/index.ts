import stringSimilarity from 'string-similarity'

export const SIMILARITY_CHECK: number = 0.70

export interface FactorInterface {
    factor: number
    value?: any
}

var defaultFactor: FactorInterface = { factor : 1.0, value: 'kg' }

export function clearCode(myString: any, keywords: Array<string> = [], size: number) : string {
    try {
        myString = myString.toLowerCase()

        keywords.forEach(keyword => {
            myString = myString.replace(keyword, "")
        })

        myString = myString.replace(/\D/g, "")
        
        let code: any = parseInt(myString.trim())

        if(isNaN(code)){
            return ''
        }

        code = code.toString()
        
        while(code.length < size){
            code = '0'+code
        }

        return code

    } catch (error) {
        console.log(error.toString())
        return myString || ''
    }
}

export function getTimestampFromCreatedAt(created_at: any) : string {
    let date = new Date(created_at)
    return ((date.getMonth() > 8) ? (date.getMonth() + 1) : ('0' + (date.getMonth() + 1))) + '/' + ((date.getDate() > 9) ? date.getDate() : ('0' + date.getDate())) + '/' + date.getFullYear()
}

export function getYear() : string {
    return new Date().getFullYear().toString()
}

export function getTimestamp() : string {
    return new Date().toLocaleDateString("en-US").toString()
}

export function checkSimilar(firstString: string, secondString: string) : number {
    let firstCleaned = getCleanString(firstString).toLowerCase()
    let secondCleaned = getCleanString(secondString).toLowerCase()
    return stringSimilarity.compareTwoStrings(firstCleaned, secondCleaned)
}

export function isSimilar(firstString: string, secondString: string, howSimilar: number = SIMILARITY_CHECK) : boolean {
    return checkSimilar(firstString, secondString) >= howSimilar
}

export function isEmpty(value: any) : boolean {
    return (value == undefined || value == null || (value && value?.trim()?.length === 0) || value?.length === 0)
}

export function parseNumber(rawNumber: any) : number{
    try {
        let n = parseFloat(rawNumber.replace("$", ""))
        if (isNaN(n)){
            return 0
        }
        return n
    } catch (error) {
        console.log(error)
        return 0
    }
}

export function compareGeneric(key: string) : Function {
    return function ( a: any, b: any ) {
        if ( a[key] < b[key] ){
          return -1
        }
        
        if ( a[key] > b[key] ){
          return 1
        }

        return 0
    }
}

export function getUnix() : string {
    return Math.round(+new Date()/1000).toString()
}

export function getCleanString(input: any) : any{
    try {
        return input.trim() || '';
    } catch (error) {
        return '';
    }
}

export function nullObject(original: any, ...args: any) : any {
    try {
        let object = original;
        args.forEach((x: any) => {
            object = object[x]
        })
        return object
    } catch (error) {
        console.log(error)
        return ''
    }
}

export function sleep(n: number) : Promise<boolean>{
    return new Promise((resolve, _) => {
        setInterval(() => {
            resolve(true)
        }, n * 1000)
    });
}

export function shuffle(array: Array<any>) : Array<any> {
    let currentIndex: number = array.length
    let temporaryValue: any = 0
    let randomIndex: any = 0

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex)
        currentIndex -= 1

        // And swap it with the current element.
        temporaryValue = array[currentIndex]
        array[currentIndex] = array[randomIndex]
        array[randomIndex] = temporaryValue
    }

    return array
}

export class ConverterBuilder {

    private inputUOM: FactorInterface = defaultFactor
    private outputUOM: FactorInterface = defaultFactor
    private inputValue: number = 0

    setInputUOM(inputUOM: FactorInterface) : ConverterBuilder{
        this.inputUOM = inputUOM
        return this
    }

    setOutputUOM(outputUOM: FactorInterface) : ConverterBuilder{
        this.outputUOM = outputUOM
        return this
    }

    setValue(inputValue: number) : ConverterBuilder{
        this.inputValue = inputValue
        return this
    }

    calculate() : number {
        return this.inputValue * (this.inputUOM.factor / this.outputUOM.factor)
    }
}