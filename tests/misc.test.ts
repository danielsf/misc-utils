import { getYear, checkSimilar, isEmpty, clearCode } from "./../src"

// Test getYear function

it('check current year if hardcoded', () => {
    expect(getYear()).toBe("2021")
})

it('check current year if not hardcoded', () => {
    expect(new Date().getFullYear()).toBe(2021)
})

it('check current year if not hardcoded with UTC', () => {
    expect(new Date().getUTCFullYear()).toBe(2021)
})

// Test checkSimilar function

it('check similarity between identical chars', () => {
    expect(checkSimilar('A', 'A')).toBeGreaterThan(0.9)
})

it('check similarity between identical strings', () => {
    expect(checkSimilar('BISCUIT', 'BISCUIT')).toBeGreaterThan(0.9)
})

it('check similarity between almost identical strings', () => {
    expect(checkSimilar('BISCUIt', 'BISCUIT')).toBeGreaterThan(0.8)
})

it('check similarity between different strings', () => {
    expect(checkSimilar('BISCUIt', 'Cascade')).toBeGreaterThan(0)
})

// Test isEmpty function

it('check emptiness between identical chars', () => {
    expect(isEmpty(' ')).toBeTruthy()
})

it('check emptiness between identical strings', () => {
    expect(isEmpty('A')).toBeFalsy()
})

it('check emptiness between almost identical strings', () => {
    expect(isEmpty(null)).toBeTruthy()
})

it('check emptiness between different strings', () => {
    expect(isEmpty(undefined)).toBeTruthy()
})

it('check emptiness between different strings', () => {
    expect(isEmpty(NaN)).toBeFalsy()
})

it('check code in the following format [code<xxxx>] ', () => {
    expect(clearCode("code 238329", ["code"], 12)).toBe("000000238329")
})

it('check code in the following format [<xxxx>-code-1] ', () => {
    expect(clearCode("238329-code-1", ["code-1"], 12)).toBe("000000238329")
})